﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatCarDNA : MonoBehaviour
{
    [SerializeField] private float speedToRun;
    [SerializeField] private float speedToRotate;

    public GameObject car;
    Rigidbody carrb;
    float rotationLimit;

    [SerializeField] float[] ray = { 0, 0, 0, 0, 0 };
    [SerializeField] float theOutput;
    [SerializeField] float dirToRotate;

    [SerializeField] private Transform visionOrigin;
    [SerializeField] private Transform[] visionDirection;
    ANN ann;

    public bool playing;
    public bool training;

    void Start()
    {
        ann = new ANN(5, 1, 2, 7, 0.2);
        carrb = car.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (this.gameObject.transform.position.y < 0)
        {
            this.transform.position = new Vector3(32, 2, 0);
        }

        this.gameObject.transform.position += ((visionDirection[2].position) - visionOrigin.position).normalized * speedToRun * Time.deltaTime;

        if (playing)
        {
            dirToRotate = 0;
            if (Input.GetKey(KeyCode.A))
            {
                dirToRotate = -3;
                this.gameObject.transform.Rotate(0, dirToRotate * speedToRotate * Time.deltaTime, 0);
            }
            if (Input.GetKey(KeyCode.D))
            {
                dirToRotate = 3;
                this.gameObject.transform.Rotate(0, dirToRotate * speedToRotate * Time.deltaTime, 0);
            }

            for (int i = 0; i < 5; i++)
            {
                ray[i] = 1000;
                RaycastHit hit;
                int layerMask = 1 << LayerMask.NameToLayer("wall");

                if (Physics.Raycast(visionOrigin.position, ((visionDirection[i].position) - visionOrigin.position), out hit, 1000f, layerMask))
                {
                    Debug.DrawRay(visionOrigin.position, ((visionDirection[i].position) - visionOrigin.position).normalized * hit.distance, Color.red);

                    ray[i] = hit.distance;
                }
            }

            List<double> outputPlay = new List<double>();

            outputPlay = Run(ray[0], ray[1], ray[2], ray[3], ray[4], dirToRotate, true);
            theOutput = (float)outputPlay[0];

            return;
        }
        else if (training)
        {
            for (int i = 0; i < 5; i++)
            {
                ray[i] = 1000;
                RaycastHit hit;
                int layerMask = 1 << LayerMask.NameToLayer("wall");

                if (Physics.Raycast(visionOrigin.position, ((visionDirection[i].position) - visionOrigin.position), out hit, 1000f, layerMask))
                {
                    Debug.DrawRay(visionOrigin.position, ((visionDirection[i].position) - visionOrigin.position).normalized * hit.distance, Color.red);

                    ray[i] = hit.distance;
                }
            }

            List<double> output = new List<double>();

            if (ray[1] > ray[3] * 3)
            {
                dirToRotate = -3;
            }
            else if (ray[1] * 3 < ray[3])
            {
                dirToRotate = 3;
            }
            else
            {
                dirToRotate = 0;
            }
            if (ray[2] < 3)
            {
                if (ray[1] > ray[3])
                {
                    dirToRotate = -7;
                }
                else
                {
                    dirToRotate = 7;
                }
            }
            output = Run(ray[0], ray[1], ray[2], ray[3], ray[4], dirToRotate, true);
            theOutput = (float)output[0];

            gameObject.transform.Rotate(0, dirToRotate * speedToRotate * 5, 0);

        }
        else
        {
            for (int i = 0; i < 5; i++)
            {
                ray[i] = 1000;
                RaycastHit hit;
                int layerMask = 1 << LayerMask.NameToLayer("wall");

                if (Physics.Raycast(visionOrigin.position, ((visionDirection[i].position) - visionOrigin.position), out hit, 1000f, layerMask))
                {
                    Debug.DrawRay(visionOrigin.position, ((visionDirection[i].position) - visionOrigin.position).normalized * hit.distance, Color.red);

                    ray[i] = hit.distance;
                }
            }

            List<double> output = new List<double>();

            if (ray[1] > ray[3] * 3)
            {
                dirToRotate = -3;
            }
            else if (ray[1] * 3 < ray[3])
            {
                dirToRotate = 3;
            }
            else
            {
                dirToRotate = 0;
            }
            if (ray[2] < 3)
            {
                if (ray[1] > ray[3])
                {
                    dirToRotate = -10;
                }
                else
                {
                    dirToRotate = 10;
                }
            }
            output = Run(ray[0], ray[1], ray[2], ray[3], ray[4], dirToRotate, false);
            theOutput = (float)output[0];

            Debug.Log("AAAAAAa");

            gameObject.transform.Rotate(0, (float)output[0] * speedToRotate * 5, 0);
        }
    }

    public List<double> Run(float ray1, float ray2, float ray3, float ray4, float ray5, double direction, bool train)
    {
        List<double> inputs = new List<double>();
        List<double> outputs = new List<double>();
        inputs.Add(ray1);
        inputs.Add(ray2);
        inputs.Add(ray3);
        inputs.Add(ray4);
        inputs.Add(ray5);
        outputs.Add(direction);
        if (train)
            return (ann.Train(inputs, outputs));
        else
            return (ann.CalcOutput(inputs, outputs));
    }
}
